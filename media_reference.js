; // media_reference.js
jQuery(document).ready(function($) {
	$('#edit-field-article-thumbnail-title-und-0-value').closest('.field-type-text').after('<div data-node-base="thumbnail" data-node-wrapper="thumbnails" id="medref-thumbnail-preview">No Media to Display</div>');
	$('#edit-field-article-main-image-title-und-0-value').closest('.field-type-text').after('<div data-node-base="main-image" data-node-wrapper="main-images" id="medref-main-image-preview">No Media to Display</div>');
	$('#edit-field-article-carousel-title-und-0-value').closest('.field-type-text').after('<div data-node-base="carousel-image" data-node-wrapper="carousel-images" id="medref-carousel-image-preview">No Media to Display</div>');
	var issueIdEl   = $('#edit-field-article-issue-und'),
		thumbsEl    = $('#medref-thumbnail-preview'),
		mainsEl     = $('#medref-main-image-preview'),
		carouselsEl = $('#medref-carousel-image-preview'),
		fetch = function() {
			var issueId = issueIdEl.val();
			if (issueId == '_none') {
				return true;
			}
			$.ajax({
				url: '/?q=api/thumbnails/' + issueId,
				success: updateT
			});
			$.ajax({
				url: '/?q=api/main-images/' + issueId,
				success: updateM
			});
			$.ajax({
				url: '/?q=api/carousel-images/' + issueId,
				success: updateC
			});
		},
		updateT = function(data) {
			updateAny(data, thumbsEl);
		},
		updateM = function(data) {
			updateAny(data, mainsEl);
		},
		updateC = function(data) {
			updateAny(data, carouselsEl);
		},
		updateAny = function(data, target) {
			var base    = target.data('node-base'),
				wrapper = target.data('node-wrapper');
			target.html('');
			if (data[wrapper] && data[wrapper].length) {
				data[wrapper].sort(function(a,b) {
					return a[base]['image'].localeCompare(b[base]['image']);
				});
				$.each(data[wrapper], function(i,o) {
					$('<img></img>')
					.attr({
						'src': o[base]['image'], 
						'alt': o[base]['image-alt'],
						'title': o[base]['image'],
						'data-title': o[base]['image-title']
					})
					.appendTo(target);
				});
			}
			highlightSelected();
		},
		clickMedia = function(e) {
			var target = $(e.target),
				input  = target.parent().prevAll('div[class*="url"]').first().find('input'),
				alt    = target.parent().prevAll('div[class*="alt"]').first().find('input'),
				title  = target.parent().prevAll('div[class*="title"]').first().find('input');
			input.val(target.attr('src'));
			alt.val(target.attr('alt'));
			title.val(target.attr('data-title'));
			highlightSelected();
		},
		highlightSelected = function() {
			var map = [
					{input: $('#edit-field-article-thumbnail-url-und-0-value').val(), wrapper: thumbsEl},
					{input: $('#edit-field-article-main-image-url-und-0-value').val(), wrapper: mainsEl},
					{input: $('#edit-field-article-carousel-image-url-und-0-value').val(), wrapper: carouselsEl}
				];
			$.each(map, function(i, o) {
				$('img', o.wrapper).css('border',0).each(function(ii, img) {
					if ($(img).attr('src') == o.input) {
						$(img).css('border', '3px solid orange');
					}
				});
			});
		}
	;
	issueIdEl.on('change', fetch);
	thumbsEl.on('click', clickMedia);
	mainsEl.on('click', clickMedia);
	carouselsEl.on('click', clickMedia);
	fetch();
});
